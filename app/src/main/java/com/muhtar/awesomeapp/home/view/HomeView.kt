package com.muhtar.awesomeapp.home.view

import com.muhtar.awesomeapp.home.model.Photo

interface HomeView {
    fun showPhotos(photos: List<Photo>)
    fun showMessage(message: String)
    fun showLoading()
    fun hideLoading()
}