package com.muhtar.awesomeapp.home

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muhtar.awesomeapp.R
import com.muhtar.awesomeapp.detail.DetailActivity
import com.muhtar.awesomeapp.home.model.Photo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_home_list.view.*

class ListPhotoAdapter(private val genres: ArrayList<Photo>, val context: Context) :
    RecyclerView.Adapter<ListPhotoAdapter.HomeViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HomeViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(
            R.layout.item_home_list, p0,
            false)

        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int = genres.size


    override fun onBindViewHolder(p0: HomeViewHolder, p1: Int) {
        p0.bindItem(genres[p1])
    }

    class HomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItem(photo: Photo) {
            val context = itemView.context
            Picasso.get().load(photo.src.original)
                .into(itemView.imageList)
            itemView.textList.setText(photo.photographer)
            itemView.setOnClickListener {
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("id", photo.id)
                intent.putExtra("name", photo.photographer)
                context.startActivity(intent)
            }
        }
    }
}
