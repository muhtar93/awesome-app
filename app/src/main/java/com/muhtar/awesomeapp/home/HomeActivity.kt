package com.muhtar.awesomeapp.home

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muhtar.awesomeapp.R
import com.muhtar.awesomeapp.home.model.Photo
import com.muhtar.awesomeapp.home.presenter.HomePresenter
import com.muhtar.awesomeapp.home.view.HomeView
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : AppCompatActivity(), HomeView {
    private var menuItem: Menu? = null
    private lateinit var presenter: HomePresenter
    private lateinit var adapterList: ListPhotoAdapter
    private lateinit var adapterGrid: GridPhotoAdapter
    private var list: ArrayList<Photo> = java.util.ArrayList()
    private var page: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(findViewById(R.id.toolbar))

        presenter = HomePresenter(this, this)
        presenter.getPhotos("nature", page)

        adapterList = ListPhotoAdapter(list, this)
        adapterGrid = GridPhotoAdapter(list, this)

        listPhoto.layoutManager = LinearLayoutManager(this)
        listPhoto.adapter = adapterList

        initListener()
    }

    private fun initListener() {
        listPhoto.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView?.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager?.itemCount
                val lastVisiblePosition = linearLayoutManager?.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition
                if (isLastPosition) {
                    page = page + 1
                    presenter.getPhotos("nature", page)
                }
            }
        })
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.item_menu, menu)
        menuItem = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.grid_option) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this,
                R.drawable.ic_grid_selected)
            menuItem?.getItem(1)?.icon = ContextCompat.getDrawable(this,
                R.drawable.ic_list_unselected)
            listPhoto.layoutManager = GridLayoutManager(this, 2)
            listPhoto.adapter = adapterGrid
        } else if (item.itemId == R.id.list_option) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this,
                R.drawable.ic_grid_unselected)
            menuItem?.getItem(1)?.icon = ContextCompat.getDrawable(this,
                R.drawable.ic_list_selected)
            listPhoto.layoutManager = LinearLayoutManager(this)
            listPhoto.adapter = adapterList
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showPhotos(photos: List<Photo>) {
        list.addAll(photos)
        adapterList.notifyDataSetChanged()
        adapterGrid.notifyDataSetChanged()
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading.visibility = View.INVISIBLE
    }
}