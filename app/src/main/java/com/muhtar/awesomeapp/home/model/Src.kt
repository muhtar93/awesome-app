package com.muhtar.awesomeapp.home.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Src (
    @SerializedName("portrait")
    @Expose
    val original: String = ""
)
