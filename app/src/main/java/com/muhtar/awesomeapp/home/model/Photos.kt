package com.muhtar.awesomeapp.home.model

import com.google.gson.annotations.SerializedName

class Photos (
    @SerializedName("photos")
    val photos: List<Photo>?
)