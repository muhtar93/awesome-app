package com.muhtar.awesomeapp.home.presenter

import android.content.Context
import com.muhtar.awesomeapp.api.PexelsApi
import com.muhtar.awesomeapp.home.model.Photos
import com.muhtar.awesomeapp.home.view.HomeView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomePresenter(val context: Context, val view: HomeView) {
    fun getPhotos(query: String, page: Int) {
        view.showLoading()
        val photos = PexelsApi.create()
        photos.listPhoto(query, page).enqueue(object : Callback<Photos> {
            override fun onFailure(call: Call<Photos>, t: Throwable) {
                view.showMessage(t.message!!)
                view.hideLoading()
            }

            override fun onResponse(call: Call<Photos>, response: Response<Photos>) {
                view.showPhotos(response.body()?.photos!!)
                view.hideLoading()
            }
        })
    }
}