package com.muhtar.awesomeapp.home.model

import com.google.gson.annotations.SerializedName

class Photo (
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("photographer")
    val photographer: String = "",
    @SerializedName("src")
    val src: Src
)
