package com.muhtar.awesomeapp.api

import com.muhtar.awesomeapp.consts.Networks
import com.muhtar.awesomeapp.detail.model.Detail
import com.muhtar.awesomeapp.home.model.Photos
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiServices {
    @Headers("Authorization: ${Networks.API_KEY}")
    @GET("search")
    fun listPhoto(
        @Query("query") query: String,
        @Query("page") page: Int): Call<Photos>

    @Headers("Authorization: ${Networks.API_KEY}")
    @GET("photos/{id}")
    fun detailPhoto(@Path("id") id: Int): Call<Detail>
}