package com.muhtar.awesomeapp.api

import com.google.gson.GsonBuilder
import com.muhtar.awesomeapp.consts.Networks
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object PexelsApi {
    var gson = GsonBuilder()
        .setLenient()
        .create()

    fun create(): ApiServices {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(Networks.BASE_URL)
            .build()
        return retrofit.create(ApiServices::class.java)
    }
}