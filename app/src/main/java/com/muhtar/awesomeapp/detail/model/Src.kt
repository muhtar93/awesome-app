package com.muhtar.awesomeapp.detail.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Src (
    @SerializedName("large")
    @Expose
    val large: String = ""
)
