package com.muhtar.awesomeapp.detail.model

import com.google.gson.annotations.SerializedName

class Detail(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("photographer")
    val photographer: String = "",
    @SerializedName("url")
    val url: String = "",
    @SerializedName("photographer_url")
    val photographer_url: String = "",
    @SerializedName("src")
    val src: Src
)