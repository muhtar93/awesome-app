package com.muhtar.awesomeapp.detail.view

import com.muhtar.awesomeapp.detail.model.Detail

interface DetailView {
    fun showDetail(detail: Detail)
    fun showMessage(message: String)
}