package com.muhtar.awesomeapp.detail

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.muhtar.awesomeapp.R
import com.muhtar.awesomeapp.detail.model.Detail
import com.muhtar.awesomeapp.detail.presenter.DetailPresenter
import com.muhtar.awesomeapp.detail.view.DetailView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity(), DetailView {
    private lateinit var presenter: DetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val id = intent.getIntExtra("id", 0)
        val name = intent.getStringExtra("name")

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setTitle("Detail ${name}")

        presenter = DetailPresenter(this, this)
        presenter.getDetail(id)
    }

    override fun showDetail(detail: Detail) {
        textDetail.setText("${detail.photographer} - ${detail.photographer_url}")
        Picasso.get().load(detail.src.large)
            .noFade()
            .into(imageDetail)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}