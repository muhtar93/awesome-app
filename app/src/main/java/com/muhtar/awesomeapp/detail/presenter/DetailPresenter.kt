package com.muhtar.awesomeapp.detail.presenter

import android.content.Context
import com.muhtar.awesomeapp.api.PexelsApi
import com.muhtar.awesomeapp.detail.model.Detail
import com.muhtar.awesomeapp.detail.view.DetailView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailPresenter(val context: Context, val view: DetailView) {
    fun getDetail(id: Int) {
        val detail = PexelsApi.create()
        detail.detailPhoto(id).enqueue(object : Callback<Detail> {
            override fun onFailure(call: Call<Detail>, t: Throwable) {
                view.showMessage(t.message!!)
            }
            override fun onResponse(call: Call<Detail>, response: Response<Detail>) {
                view.showDetail(response.body()!!)
            }
        })
    }
}